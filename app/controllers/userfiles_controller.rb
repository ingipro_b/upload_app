class UserfilesController < ApplicationController

  def create
    @userfile = Userfile.new
    @userfile.uf_file.attach(params[:userfile][:uf_file])

    if @userfile.save
      respond_to do |format|
        format.html { redirect_to root_path }
        format.turbo_stream
      end
    else
      render :index
    end
  end

  def index
    @userfiles = Userfile.all
  end

  def new
    @userfile = Userfile.new
  end

  def show
    @userfile = Userfile.find(params[:id])
    redirect_to rails_blob_path(@userfile.uf_file, disposition: "attachment")
  end

end
 