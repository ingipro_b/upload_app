class CreateUserfiles < ActiveRecord::Migration[7.1]
  def change
    create_table :userfiles do |t|
      t.string :uf_name

      t.timestamps
    end
  end
end
